import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  form!: FormGroup;

  toggleBool: any=true;
  radiotoggle: boolean = true;
  dateValid: boolean = true;




  ngOnInit() {

  this.form = new FormGroup({
    'ename' : new FormControl(null, Validators.required),
    'Description' : new FormControl(null, [Validators.required, Validators.minLength(10)]),
    'gender': new FormControl(null, [Validators.required]),
    'hoby': new FormControl(null, [Validators.required]),
    'dob': new FormControl(null, [Validators.required]),

  });
  }

  submit() {
    console.log(this.form.value);
    this.form.reset();
  }

  get ename() {
    return this.form.get('ename');
  }

  get Description() {
    return this.form.get('Description');
  }

  changeEventCheckbox(event:any) {
    if (event.target) {
        this.toggleBool= false;
    }
    else {
        this.toggleBool= true;
    }
}

changeEventRadio(event:any) {
  if (event.target.checked) {
      this.radiotoggle= false;
  }
  else {
      this.radiotoggle= true;
  }
}

date(event:any){
  if(event.target){
this.dateValid = false;
  }
  else{
    this.dateValid = true;
  }
}
}


